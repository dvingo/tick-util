https://clojars.org/dv/tick-util

[![Clojars Project](https://img.shields.io/clojars/v/dv/tick-util.svg)](https://clojars.org/dv/tick-util)

Extracted from an app, contains some helpers for tick.

Build a deployable jar of this library:

    $ clojure -A:jar

Install it locally:

    $ clojure -A:install

Deploy it to Clojars -- needs `CLOJARS_USERNAME` and `CLOJARS_PASSWORD` environment variables:

    $ clojure -A:deploy
